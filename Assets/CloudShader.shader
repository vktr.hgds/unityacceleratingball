﻿


//2 languages
//HLSL
//CG


//Shader is an interface for materials

//HLSL starting...


Shader "Unlit/CloudShader"
{

	//properties of the shader
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color ("Colour", Color) = (1,1,1)
    }
    SubShader
    {

        Pass
        {
			//CG Language
            CGPROGRAM

			//create function names first
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
				//pos of the vertex that we currently working on
                float4 vertex : POSITION;

				//texture is 2d - how to put the texture around the object
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)

				//pos of the vertex that we currently working on
                float4 vertex : SV_POSITION;
            };

			//references to the paramteres inside the CG code (properties)
            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
				//Object posiiton and shows in the camera
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }


            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}

