﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject ball;
    //distance between camera & ball
    Vector3 offset;

    //smoothly follows object
    public float followSpeed;

    [SerializeField]
    public bool theEnd;

    // Start is called before the first frame update
    void Start()
    {
        //attach it to the ball
        offset = ball.transform.position - transform.position;
        theEnd = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!theEnd)
        {
            FollowBall();
        }
    }

    void FollowBall()
    {
        Vector3 pos = transform.position;
        Vector3 targetPos = ball.transform.position - offset;

        //moves from one value to another value with the speed of 'followSpeed'
        pos = Vector3.Lerp(pos, targetPos, followSpeed * Time.deltaTime);
        transform.position = pos;
    }
}
