﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject particle;
    public GameObject death;
    public GameObject minusX;

    [SerializeField]
    private float speed = 8;
    bool started;
    Rigidbody rb;
    bool theEnd;

    [SerializeField]
    bool jump = true;

    [SerializeField]
    float height = 300;

    bool cameraCounter = true;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        started = false;
        theEnd = false;
    }

    void FixedUpdate()
    {
        if (!started)
        {
            if (Input.GetKeyDown("space"))
            {
                GoAhead();
                started = true;
                GameUI.instance.StartGame();
            }
        }
        else
        {

            //Debug.DrawRay(transform.position, Vector3.down, Color.red);

            if (!Physics.Raycast(transform.position, Vector3.down, 3f))
            {
                theEnd = true;
                rb.velocity = new Vector3(0f, -10f, 0f);

                //ha leesik a labda, a kamera ne kovesse
                Camera.main.GetComponent<CameraController>().theEnd = true;
                GameUI.instance.GameOver();

            }

            if (Input.GetKeyDown("a") && !theEnd)
            {
                speed += 0.05f;
                TurnLeft();
                ScoreManager.instance.switchDirectionScore();
            }
            if (Input.GetKeyDown("d") && !theEnd)
            {
                speed += 0.05f;
                TurnRight();
                ScoreManager.instance.switchDirectionScore();
            }
            if (Input.GetKeyDown("s") && !theEnd)
            {
                speed += 0.05f;
                GoBack();
            }
            if (Input.GetKeyDown("w") && !theEnd)
            {
                speed += 0.05f;
                GoAhead();
                ScoreManager.instance.switchDirectionScore();
            }

            /*
            if (Input.GetKeyDown("c"))
            {
                if (cameraCounter)
                {
                    Camera.main.GetComponent<CameraController>().enabled = false;
                    Camera cam2 = GetComponent<SecondCamera>().enabled = true;
                    cameraCounter = false;
                } else {
                    cameraCounter = true;
                }
               
            }
            */

            /*
            if (Input.GetMouseButtonDown(0) && !theEnd)
            {
                if (jump)
                {
                    Jump();
                    jump = false;
                } else
                {
                    GoDown();
                    jump = true;
                }
                
            }
            */
            
        }
        
    }

    void TurnLeft()
    {
        rb.velocity = new Vector3(-speed, 0, 0);
    }

    void TurnRight()
    {
        rb.velocity = new Vector3(speed, 0, 0);
    }

    void GoAhead()
    {
        rb.velocity = new Vector3(0, 0, speed);
    }

    void GoBack()
    {
        rb.velocity = new Vector3(0, 0, -speed);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (started)
        {
            if (collider.gameObject.tag == "PickUp")
            {
                Instantiate(particle, collider.gameObject.transform.position, Quaternion.identity);
                ScoreManager.instance.pickUpObject();
                Destroy(collider.gameObject);
            }

            if (collider.gameObject.tag == "X")
            {
                Instantiate(minusX, collider.gameObject.transform.position, Quaternion.identity);
                ScoreManager.instance.decrementScore();
                Destroy(collider.gameObject);
            }
            else if (collider.gameObject.tag == "Capsule")
            {
                Instantiate(death, collider.gameObject.transform.position, Quaternion.identity);
                Destroy(collider.gameObject);
                GameUI.instance.GameOver();
                //speed = 0;
                theEnd = true;
                //Destroy(gameObject);
            }
            else if (collider.gameObject.tag == "Tree")
            {
                Destroy(collider.gameObject, 4f);
            }
            else if (collider.gameObject.tag == "SmallTree")
            {
                Destroy(collider.gameObject);
            }
            else if (collider.gameObject.tag == "Mushroom")
            {
                Destroy(collider.gameObject, 15f);
            }
        }
        
    }

    /*
    void Jump()
    {
        height = 300;
        Vector3 jump = new Vector3(0.0f, height, 0.0f);
        rb.AddForce(jump);
    }

    void GoDown()
    {
        height = 100;
        Vector3 jump = new Vector3(0.0f, height, 0.0f);
        rb.AddForce(jump);
    }
    */

    /*

    [SerializeField]
    private float speed = 5;
    private Rigidbody rb;
    private int count;

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * 20);

        if (Input.GetMouseButtonDown(0))
        {
            Jump();
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Jump()
    {
        Vector3 jump = new Vector3(0.0f, 500.0f, 0.0f);
        rb.AddForce(jump);
    }

    */


}
