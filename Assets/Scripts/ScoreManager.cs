﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    //singleton class -> one instance to control every methods
    public static ScoreManager instance;
    public int score;
    public int highScore;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        //store our score
        PlayerPrefs.SetInt("score", score);
    }

    // Update is called once per frame
    void Update()
    {
        //startScore();
    }


    public void incrementScore()
    {
        score++;
    }

    public void switchDirectionScore()
    {
        score += 3; ;
    }

    public void decrementScore()
    {
        score -= 50;
    }

    public void pickUpObject()
    {
        score += 10;
    }

    //increment our score every 0.5secs
    public void startScore()
    {
        InvokeRepeating("incrementScore", 0.0f, 0.3f);
    }

    public void endScore()
    {
        CancelInvoke("incrementScore");
        PlayerPrefs.SetInt("score", score);

        //check if current score bigger the stored highscore -> if yes, set the current score as high score
        if (PlayerPrefs.HasKey("highScore"))
        {
            if (score > PlayerPrefs.GetInt("highScore"))
            {
                PlayerPrefs.SetInt("highScore", score);
            }
        } else
        {
            PlayerPrefs.SetInt("highScore", score);
        }
    }

}
