﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChecker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Ball")
        {
            //call this function after 0.5 secs
            //gameObject.GetComponent<Renderer>().material.color = Color.red;
            Invoke("MoveAway", 0.5f);
            MoveAway();
           
        }
    }

    void MoveAway()
    {
        GetComponentInParent<Rigidbody>().useGravity = true;
        GetComponentInParent<Rigidbody>().isKinematic = false;

        //change color whenn falling down
        GetComponentInParent<Renderer>().material.color = Color.Lerp(Color.red, Color.green, 0.1f);

        //destroy the platform itself, not just the trigger
        Destroy(transform.parent.gameObject, 2f);
    }

}
