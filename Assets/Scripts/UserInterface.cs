﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine.SceneManagement;
using UnityEngine;

public class UserInterface : MonoBehaviour
{
    //for animations
    //singleton class -> one instance to control every methods
    public static UserInterface instance;
    public GameObject accBallPanel;
    public GameObject gameOverPanel;
    public GameObject startText;
    public Text score;
    public Text inGameScore;
    public Text highScore1;
    public Text highScore2;
    public GameObject newMapButton;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            highScore1.text = "Highscore: " + PlayerPrefs.GetInt("highScore").ToString();
            inGameScore.gameObject.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        //inGameScore.text = "Score: " + PlayerPrefs.GetInt("score").ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        //
        
    }

    public void GameStart()
    {
        startText.GetComponent<Animator>().Play("textDown");
        highScore1.text = "Highscore: " + PlayerPrefs.GetInt("highScore").ToString();
        startText.SetActive(false);
        accBallPanel.GetComponent<Animator>().Play("PanelFlow");
        newMapButton.SetActive(false);
        //inGameScore.gameObject.SetActive(true);
    }

    public void GameOver()
    {
        highScore2.text = PlayerPrefs.GetInt("highScore").ToString();
        score.text = PlayerPrefs.GetInt("score").ToString();
        gameOverPanel.SetActive(true);
    }

    //load the first (and only) scene of the game
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    // Update is called once per frame
   
}
