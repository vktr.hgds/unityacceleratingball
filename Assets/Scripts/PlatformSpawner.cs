﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{

    //ezeket az object-et fogjuk letrehozni
    public GameObject platform;
    public GameObject cloud;
    public GameObject pickups;
    public GameObject capsules;
    public GameObject trees;
    public GameObject smalltrees;
    public GameObject bigtrees;
    public GameObject X;
    //public GameObject mushrooms;
    //public GameObject grass;

    Vector3 lastPosition;
    float size;

    //check if last 2 numbers are same or not
    
    int[] numbers = new int[1000];
    public bool theEnd;

    [SerializeField]
    int rand;
    // Start is called before the first frame update
    void Start()
    {
        lastPosition = platform.transform.position;
        size = platform.transform.localScale.x;
        
        CoordinateZ();
        SpawnPlatforms();
    }

    // Update is called once per frame
    void Update()
    {
        if (theEnd)
        {
            CancelInvoke("CoordinateZ");
            CancelInvoke("CoordinateMinusX");
            CancelInvoke("CoordinatePlusX");
        }
    }

    void SpawnPlatforms()
    {
        for (int i = 1; i < 650; i++)
        {
            rand = Random.Range(0, 3);
            Debug.Log("i = " + i + ", rand = " + rand);
            //balra
            if (rand == 0)
            {
                //ha balra tenne le, de az elozo jobb volt, akkor ujrageneralas
                numbers[i] = rand;
                Debug.Log(" -- numbers[i] = " + numbers[i]);
                if (numbers[i - 1] == 1)
                {
                    Debug.Log("numbers[i-1] = " + numbers[i - 1]);
                    /*
                    int newRand = Random.Range(0, 2);
                    if (newRand == 1)
                    {
                        CoordinatePlusX();
                    } else
                    {
                        CoordinateZ();
                    }
                    
                    */
                    CoordinateZ();
                   // Invoke("CoordinateZ", 0.2f);
                } else
                {
                    CoordinateMinusX();
                    //Invoke("CoordinateMinusX", 0.2f);
                }
               
            }

            // jobbra
            else if (rand == 1)
            {
                numbers[i] = rand;
                Debug.Log(" -- numbers[i] = " + numbers[i]);
                if (numbers[i - 1] == 0)
                {
                    Debug.Log("numbers[i-1] = " + numbers[i - 1]);
                    /*
                    int newRand = Random.Range(0, 2);
                    if (newRand == 1)
                    {
                        CoordinateMinusX();
                    }
                    else
                    {
                        CoordinateZ();
                    }
                    */
                    CoordinateZ();
                    //Invoke("CoordinateZ", 0.2f);
                } else
                {
                    CoordinatePlusX();
                   // Invoke("CoordinatePlusX", 0.2f);
                }

            }

            //felfele
            else if (rand == 2)
            {
                numbers[i] = rand;
                CoordinateZ();
                Destroy(transform.gameObject, 2f);
                //Invoke("CoordinateZ", 0.2f);
            }

            if (theEnd)
            {
                return;
            }

            //InvokeRepeating("CoordinateMinusX", 2f, 0.2f);
            //InvokeRepeating("CoordinatePlusX", 2f, 0.2f);
           // InvokeRepeating("CoordinateZ", 2f, 0.2f);
        }

        
    }


    //spawns on axis x -> towards right
    void CoordinatePlusX()
    {
        Vector3 pos = lastPosition;
        pos.x += size;
        lastPosition = pos;
        Instantiate(platform, pos, Quaternion.identity);

        int random = Random.Range(0, 50);
        if (random <= 4)
        {
            Instantiate(pickups, new Vector3(pos.x, pos.y + 1, pos.z), pickups.transform.rotation);
        }
        else if (random == 5)
        {
            Instantiate(capsules, new Vector3(pos.x, pos.y + 1.5f, pos.z), capsules.transform.rotation);
        }
        else if (random > 20 && random < 50)
        {
            int XPos = 25;
            int ZPos = 5;

            for (int i = 0; i < 4; i++)
            {
                /*
                 int newRandom = Random.Range(0, 2);

                 if (newRandom == 0)
                 {*/
                if (i % 2 == 0)
                {
                    Instantiate(trees, new Vector3(pos.x + XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                    XPos += 15;
                    ZPos += 7;
                }
                else if (i % 2 == 1)
                {
                    Instantiate(smalltrees, new Vector3(pos.x + XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                    XPos += 15;
                    ZPos += 7;
                }
               
                /*
                } else
                {
                    Instantiate(trees, new Vector3(pos.x + XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                    XPos += 15;
                    ZPos += 7;
                    
                }
                */
            }

            /*
            int GrassX = 10;
            int GrassZ = 3;

            for (int i = 0; i < 3; i++)
            {
                Instantiate(grass, new Vector3(pos.x + GrassX, pos.y - 5, pos.z + GrassZ), grass.transform.rotation);
                GrassX += 5;
                GrassZ += 3;
            }
            */
        }
    }

    //spawns on axis x -> towards left
    void CoordinateMinusX()
    {
        Vector3 pos = lastPosition;
        pos.x -= size;
        lastPosition = pos;
        Instantiate(platform, pos, Quaternion.identity);

        int random = Random.Range(0, 50);
        if (random <= 4)
        {
            Instantiate(pickups, new Vector3(pos.x, pos.y + 1, pos.z), pickups.transform.rotation);
        }
        else if (random == 5)
        {
            Instantiate(capsules, new Vector3(pos.x, pos.y + 1.5f, pos.z), capsules.transform.rotation);
        } else if (random > 20 && random < 50)
        {
            int XPos = 25;
            int ZPos = 5;

            for (int i = 0; i < 3; i++)
            {
                /*
                int newRandom = Random.Range(0, 2);

                if (newRandom == 0)
                {*/
                if (i % 2 == 0)
                {
                    Instantiate(smalltrees, new Vector3(pos.x + XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                    Instantiate(cloud, new Vector3(pos.x + XPos, pos.y + 12, pos.z + ZPos), Quaternion.identity);
                    XPos += 15;
                    ZPos += 7;

                    

                }
                else if (i % 2 == 1)
                {
                    Instantiate(trees, new Vector3(pos.x + XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                    XPos += 15;
                    ZPos += 7;
                }
               
                /*
                } else
                {
                    Instantiate(trees, new Vector3(pos.x + XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                    XPos += 15;
                    ZPos += 7;
                    
                }
                */

            }
            /*

            int GrassX = 10;
            int GrassZ = 3;

            for (int i = 0; i < 3; i++)
            {
                Instantiate(grass, new Vector3(pos.x + GrassX, pos.y - 5, pos.z + GrassZ), grass.transform.rotation);
                GrassX += 5;
                GrassZ += 3;
            }
            */
        }
    }

    //spawns on axis z
    void CoordinateZ()
    {
        Vector3 pos = lastPosition;
        pos.z += size;
        lastPosition = pos;
        Instantiate(platform, pos, Quaternion.identity);

        int random = Random.Range(0, 50);
        if (random <= 4)
        {
            Instantiate(pickups, new Vector3(pos.x, pos.y + 1, pos.z), pickups.transform.rotation);
        }
        else if (random == 5)
        {
            Instantiate(capsules, new Vector3(pos.x, pos.y + 1.5f, pos.z), capsules.transform.rotation);
            
        }
        else if (random > 20 && random < 50)
        {
            if (random > 20 && random < 30)
            {
                Instantiate(X, new Vector3(pos.x, pos.y + 1.5f, pos.z), X.transform.rotation);
                Instantiate(cloud, new Vector3(pos.x , pos.y + 9, pos.z ), Quaternion.identity);
            }

            int XPos = 25;
            int ZPos = 5;

            for (int i = 0; i < 6; i++)
            {
                /*
                int newRandom = Random.Range(0, 2);

                if (newRandom == 0)
                {*/
                if (i < 3)
                {
                    if (i % 2 == 0)
                    {
                        Instantiate(smalltrees, new Vector3(pos.x - XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                        Instantiate(cloud, new Vector3(pos.x + XPos, pos.y + 9, pos.z + ZPos), Quaternion.identity);
                        XPos += 15;
                        ZPos += 7;
                      
                     

                    }
                    else if (i % 2 == 1)
                    {
                        Instantiate(trees, new Vector3(pos.x - XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                        Instantiate(cloud, new Vector3(pos.x - XPos, pos.y + 8.5f, pos.z - ZPos), Quaternion.identity);
                        XPos += 15;
                        ZPos += 7;
                       
                    }
                   
                } else
                {
                    if (i % 2 == 0)
                    {
                        Instantiate(trees, new Vector3(pos.x - XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                        Instantiate(cloud, new Vector3(pos.x - XPos, pos.y + 9.5f, pos.z - ZPos), Quaternion.identity);
                        XPos += 15;
                        ZPos += 7;
                    }
                    else if (i % 2 == 1)
                    {
                        Instantiate(smalltrees, new Vector3(pos.x - XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                        XPos += 15;
                        ZPos += 7;
                    }
                   
                }
                

                /*
                } else
                {
                    Instantiate(trees, new Vector3(pos.x + XPos, pos.y - 0.8f, pos.z + ZPos), Quaternion.identity);
                    XPos += 15;
                    ZPos += 7;
                    
                }
                */

            }

            int newXPos = 25;
            int newZPos = 5;

            for (int j = 0; j < 2; j++)
            {
                if (j == 0)
                {
                    Instantiate(trees, new Vector3(pos.x + newXPos, pos.y - 0.8f, pos.z + newZPos), Quaternion.identity);
                    newXPos += 15;
                    newZPos += 7;
                } else
                {
                    Instantiate(smalltrees, new Vector3(pos.x + newXPos, pos.y - 0.8f, pos.z + newZPos), Quaternion.identity);
                    newXPos += 15;
                    newZPos += 7;
                }
                
            }

            /*
            int GrassX = 10;
            int GrassZ = 3;

            for (int i = 0; i < 3; i++)
            {
                Instantiate(grass, new Vector3(pos.x - GrassX, pos.y - 5, pos.z + GrassZ), grass.transform.rotation);
                GrassX += 5;
                GrassZ += 3;
            }
            */
        }
    }

}
