﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    //singleton class, GameUI manages the UI and the game mechanism itself
    public static GameUI instance;
    public bool isGameOver;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    { 
        UserInterface.instance.GameStart();
        ScoreManager.instance.startScore();
    }

    public void GameOver()
    {
        UserInterface.instance.GameOver();
        ScoreManager.instance.endScore();
    }
}
